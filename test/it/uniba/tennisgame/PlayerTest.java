package it.uniba.tennisgame;

import static org.junit.Assert.*;

import org.junit.Test;

public class PlayerTest {

	@Test
	public void scoreShouldBeIncreased() {
		//Arrange
		Player player = new Player("Federer", 0);
		//Act
		player.incrementScore();
		//Assert
		assertEquals(1, player.getScore());
	}

	@Test
	public void scoreShouldNotBeIncreased() {
		//Arrange
		Player player = new Player("Federer", 0);
		//Assert
		assertEquals(0, player.getScore());
	}
	
	@Test
	public void scoreShouldBeLove() {
		//Arrange
		Player player = new Player("Federer", 0);
		//Act
		String scoreAsString = player.getScoreAsString();
		//Assert
		assertEquals("love", scoreAsString);
	}
}
